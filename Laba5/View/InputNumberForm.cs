﻿using System;
using System.Windows.Forms;

namespace Laba5.View
{
    public partial class InputNumberForm : Form
    {
        public InputNumberForm()
        {
            InitializeComponent();
            lbNotice.Text = "The students only from the group with\n" +
                "this number will be in students table.";
            FormBorderStyle = FormBorderStyle.FixedDialog;
            numericUpDown.Maximum = UInt32.MaxValue;

            btnOk.Click += BtnOk_Click; 
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            uint key = Convert.ToUInt32(numericUpDown.Value);

            if (!Model.Storage.Instance.Groups.ContainsKey(key))
            {
                MessageBox.Show(this, Model.ErrorMessages.NO_SUCH_KEY,
                    Model.ErrorMessages.NO_SUCH_KEY, MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

            Model.DisplayMode.ChosenGroupNumber = key;
            DialogResult = DialogResult.OK;

        }

        public InputNumberForm SetExplanation(string expl)
        {
            lbNotice.Text = expl;
            return this;
        }

        public InputNumberForm SetTitle(string title)
        {
            Text = title;
            return this;
        }
    }
}
