﻿using System;
using System.Windows.Forms;

namespace Laba5.View
{
    public partial class AddStudentForm : Form
    {
        public AddStudentForm()
        {
            InitializeComponent();
            btnCancel.Click += (object sender, EventArgs e) =>
            {
                DialogResult = DialogResult.Cancel;
            };
            btnAdd.Click += BtnAdd_Click;

            nudGrNum.Minimum = 1;
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbName.Text)
                || string.IsNullOrEmpty(tbSurname.Text))
            {
                MessageBox.Show(this, "Surname or name can not be empty!!!",
                    "Empty field", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            var stud = new Model.Student(tbSurname.Text, tbName.Text, 
                                        Convert.ToUInt32(nudGrNum.Value));
            try
            {
                Model.Storage.AddStudent(stud);
            } catch (ArgumentException ex)
            {
                MessageBox.Show(this, ex.Message, ex.Message, 
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DialogResult = DialogResult.OK;
        }
    }
}
