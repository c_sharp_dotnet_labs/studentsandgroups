﻿using System;
using System.Windows.Forms;

namespace Laba5.View
{
    public partial class AddGroupForm : Form
    {
        public bool ChangeMode { get; set; }
        public Model.Group ResultGroup { get; set; }
        public AddGroupForm()
        {
            InitializeComponent();
            numericUpDown.Minimum = 1;
            btnCancel.Click += (object sender, EventArgs e) =>
            {
                DialogResult = DialogResult.Cancel;
            };
            btnAdd.Click += BtnAdd_Click;
            ResultGroup = new Model.Group();
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            uint number = Convert.ToUInt32(numericUpDown.Value);
            if (!ChangeMode && Model.Storage.Instance.Groups.ContainsKey(number))
            {
                MessageBox.Show(this, Model.ErrorMessages.NOT_UNIQUE_KEY, 
                    Model.ErrorMessages.NOT_UNIQUE_KEY, MessageBoxButtons.OK, 
                    MessageBoxIcon.Error);
            }
            ResultGroup.Number = number;
            ResultGroup.Name = tbName.Text;
            DialogResult = DialogResult.OK;
        }

        public AddGroupForm SetValues(Model.Group group)
        {
            numericUpDown.Value = group.Number;
            tbName.Text = group.Name;
            return this;
        }
        public AddGroupForm SetTitle(string title)
        {
            Text = title;
            return this;
        }
        public AddGroupForm SetBtnText(string text)
        {
            btnAdd.Text = text;
            return this;
        }
    }
}
