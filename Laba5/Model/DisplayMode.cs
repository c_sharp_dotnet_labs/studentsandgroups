﻿
namespace Laba5.Model
{
    enum Filter
    {
        All, ByGroupNumber
    }

    enum Sort
    {
        BySurname, ByGroup
    }

    static class DisplayMode
    {
        public static Filter Filter { get; set; }
        public static uint ChosenGroupNumber { get; set; }

        public static Sort Criterion { get; set; }
    }
}
