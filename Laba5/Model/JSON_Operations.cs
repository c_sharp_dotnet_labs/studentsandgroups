﻿using System.IO;
using Newtonsoft.Json;

namespace Laba5.Model
{
    static class JSON_Operations
    {
        public static void SaveStorage(Stream stream)
        {
            using (var file = new StreamWriter(stream))
            {
                file.WriteLine(JsonConvert.SerializeObject(
                    Storage.Instance, Newtonsoft.Json.Formatting.Indented
                    ));
            }
        }
        public static void LoadStorageFromJsonText(string jsonText)
        {
            Storage.Instance = JsonConvert.DeserializeObject<Storage>(jsonText);
        }

    }
}
