﻿namespace Laba5.Model
{
    class Student
    {
        public uint ID { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public uint GroupNumber { get; set; }
        public string phoneNumber { get; set; }

        public Student(string surname, string name, uint groupNumber)
        {
            Surname = surname; Name = name; GroupNumber = groupNumber;
        }

        public static int ComparisonBySurname(Student a, Student b)
        {
            return a.Surname.CompareTo(b.Surname);
        }

        public static int ComparisonByGroup(Student a, Student b)
        {
            if (a.GroupNumber < b.GroupNumber)
            {
                return -1;
            } else if ( a.GroupNumber == b.GroupNumber)
            {
                return 0;
            } else
            {
                return 1;
            }
        }

    }
}
